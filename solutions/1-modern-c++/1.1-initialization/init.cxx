#include <iostream>
#include <string>
#include <set>

using namespace std;
using namespace std::literals;


int main() {
    {
        long double pi{3.1415926};;
        cout << "(1) pi: " << pi << endl;
    }
    {
        int number;
        cout << "(2) number: " << number << endl;
    }
    {
        int number{};
        cout << "(3) number: " << number << endl;
    }

    string   haystack = "This is a sample text string with fake data"s;
    if (int pos      = haystack.find("sa"s); pos != string::npos) {
        cout << "found: " << haystack.substr(pos, 11) << endl;
    }
    if (int pos      = haystack.find("fa"s, 20); pos != string::npos) {
        cout << "found: " << haystack.substr(pos) << endl;;
    }

    set<string> words = {"cool"s, "it"s, "C++"s, "is"s};
    for (string const& w : words) cout << w << " ";
    cout << endl;

    return 0;
}
