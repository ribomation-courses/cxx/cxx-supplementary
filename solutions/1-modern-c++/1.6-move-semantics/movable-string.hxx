#pragma once

#include <iostream>
#include <cstring>
#include <unistd.h>

char* _end;

static bool isHeap(void const* addr) {
    return (&_end <= addr) && (addr < sbrk(0));
}

void* operator new(size_t numBytes) {
    void* addr = malloc(numBytes);
    std::cerr << "*** new: " << numBytes << " bytes --> " << addr << std::endl;
    return addr;
}

void operator delete(void* addr) {
    std::cerr << "*** delete: " << addr << "\n";
    free(addr);
}

void operator delete(void* addr, std::size_t numElems) {
    std::cerr << "*** delete: " << addr << ", count: " << numElems << std::endl;
    free(addr);
}

namespace ribomation {
    using namespace std;


    class TinyString {
        char const* payload = nullptr;

    public:
        TinyString() = delete;
        TinyString(const TinyString&) = delete;
        TinyString& operator =(const TinyString&) = delete;

        TinyString(const char* s) : payload{s} {
            cout << "TinyString(char* '" << payload << "') @ " << this << endl;
        }

        ~TinyString() {
            cout << "~TinyString() @ " << this;
            if (payload && isHeap(payload)) {
                cout << " DELETE \"" << payload << "\" @ " << reinterpret_cast<unsigned long>(payload) << endl;
                delete payload;
            } else {
                cout << " no payload";
            }
            cout << endl;
        }

        TinyString(TinyString&& that) : payload(that.payload) {
            cout << "TinyString(&& '" << payload << "') @ " << this << endl;
            that.payload = nullptr;
        }

        TinyString& operator =(TinyString&& that) {
            cout << "TinyString::operator=(&& '" << payload << "') @ " << this;
            if (this != &that) {
                if (payload && isHeap(payload)) {
                    cout << " DELETE \"" << payload << "\"\n";
                    delete payload;
                }
                payload = that.payload;
                that.payload = nullptr;
            }
            cout << endl;
            return *this;
        }

        friend std::ostream& operator <<(std::ostream& os, const TinyString& s) {
            return os << '"' << (s.payload != nullptr ? s.payload : "") << "\" @ "
                      << reinterpret_cast<unsigned long>(s.payload);
        }

        friend TinyString operator +(const TinyString& lhs, const TinyString& rhs) {
            char* buf = new char[1 + strlen(lhs.payload) + strlen(rhs.payload)];
            strcpy(buf, lhs.payload);
            strcat(buf, rhs.payload);
            return {buf};
        }

    };
}
