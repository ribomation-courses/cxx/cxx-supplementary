#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int        numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    const auto N         = sizeof(numbers) / sizeof(numbers[0]);

    const auto factor    = 42U;
    transform(numbers, numbers + N, numbers, [=](auto n) {
        return factor * n;
    });

    for_each(numbers, numbers + N, [](auto n){
        cout << n << endl;
    });

    return 0;
}
