#include <iostream>
#include <vector>
#include <numeric>
#include <cmath>

using namespace std;

int main() {
    vector<int> numbers {
            5, 1, 10, -3, 4, 2, -10, 12, 17, 2, 2, 2, 2, 5, 5, 5, 5, 5
    };
    auto N = numbers.size();

    auto mean = accumulate(numbers.begin(), numbers.end(), 0, [](auto sum, auto n) {
        return sum + n;
    }) / static_cast<double>(N);

    auto stddev = sqrt(accumulate(numbers.begin(), numbers.end(), 0, [=](auto sum, auto n) {
        auto sqsum = [=](auto x) {
            return (x - mean) * (x - mean);
        };
        return sum + sqsum(n);
    }) / static_cast<double>(N - 1));

    cout << "# values: " << N << endl;
    cout << "mean    : " << mean << endl;
    cout << "stddev  : " << stddev << endl;

    return 0;
}