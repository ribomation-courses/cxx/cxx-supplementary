#include <iostream>
#include <functional>

using namespace std;

auto reduce(int numbers[], unsigned n, function<int(int, int)> f) {
    auto accumulated = numbers[0];

    for (auto k = 1U; k < n; ++k) {
        accumulated = f(accumulated, numbers[k]);
    }

    return accumulated;
}

int main() {
    int            arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
    constexpr unsigned N     = sizeof(arr) / sizeof(int);

    cout << "SUM : " << reduce(arr, N, [](int acc, int n) {
        return acc + n;
    }) << endl;

    cout << "PROD: " << reduce(arr, N/2, [](int acc, int n) {
        return acc * n;
    }) << endl;

    cout << "MAX : " << reduce(arr, N, [](int acc, int n) {
        return max(acc, n);
    }) << endl;


    return 0;
}





