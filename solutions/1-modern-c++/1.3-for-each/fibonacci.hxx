#pragma once

class Fibonacci {
    unsigned n;
public:
    Fibonacci(unsigned n) : n{n} {}
    ~Fibonacci() = default;
    Fibonacci(const Fibonacci&) = default;
    Fibonacci& operator =(const Fibonacci&) = default;

    struct iterator {
        unsigned long f2 = 0;
        unsigned long f1 = 1;
        unsigned      n;

        iterator(unsigned n) : n{n} {}
        bool          operator !=(iterator& rhs) { return n != rhs.n; }
        unsigned long operator *() { return f1; }
        void          operator ++() {
            unsigned long f = f2 + f1;
            f2 = f1;
            f1 = f;
            ++n;
        }
    };

    iterator begin() const { return {0}; }
    iterator end()   const { return {n}; }
};
