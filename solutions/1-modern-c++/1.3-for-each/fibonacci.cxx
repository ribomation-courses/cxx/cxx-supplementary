#include <iostream>
#include <iomanip>
#include <string>
#include "fibonacci.hxx"

using namespace std;
using namespace std::literals;

int main(int argc, char** argv) {
    auto n = argc == 1 ? 92U : stoi(argv[1]);

    for (auto f : Fibonacci{n})
        cout << setw(20) << f << endl;

    return 0;
}
