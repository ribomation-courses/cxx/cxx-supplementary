#include <iostream>
#include <memory>

using namespace std;

struct Account {
    double balance;

    Account() {
        cout << "Account() @ " << this << endl;
    }

    Account(int n) : balance(n) {
        cout << "Account(" << balance << ") @ " << this << endl;
    }

    ~Account() {
        cout << "~Account() @ " << this << endl;
    }

    Account(const Account& that) {
        cout << "Account(const Account&) @ " << this << endl;
        balance = that.balance;
    }

    Account& operator=(const Account&) = delete;
};

ostream& operator<<(ostream& os, Account& a) {
    return os << "Account{" << a.balance << "}";
}


shared_ptr<Account> func(shared_ptr<Account> p) {
    cout << "[func] " << *p << ", cnt=" << p.use_count() << endl;
    p->balance += 150;
    cout << "[func] " << *p << ", cnt=" << p.use_count() << endl;
    return p;
}

int main() {
    auto a = make_shared<Account>(500);
    cout << "[main] " << *a << ", cnt=" << a.use_count() << endl;
    {
        auto q = make_shared<Account>(100);
        cout << "[main] " << *q << ", cnt=" << q.use_count() << endl;
        {
            auto x = q;
            cout << "[blck] " << *x << ", cnt=" << x.use_count() << endl;
            auto y = func(x);
            cout << "[blck] " << *y << ", cnt=" << y.use_count() << endl;
            a = q;
        }
        cout << "[main] " << *q << ", cnt=" << q.use_count() << endl;
    }
    cout << "[main] " << *a << ", cnt=" << a.use_count() << endl;
    return 0;
}