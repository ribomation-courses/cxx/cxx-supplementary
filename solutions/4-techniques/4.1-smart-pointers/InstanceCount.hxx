#pragma  once

class InstanceCount {
    static int count;
    //N.B. don't forget to allocate space for the counter in a *.cxx file
    //int InstanceCount::count = 0;

protected:
    InstanceCount() { count++; }

public:
    virtual ~InstanceCount() { count--; }

    static int numObjs() { return count; }
};
