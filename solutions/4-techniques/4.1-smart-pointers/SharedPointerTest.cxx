#include <memory>
#include "Account.hxx"

int InstanceCount::count = 0;

shared_ptr<Account> func2(shared_ptr<Account> q) {
    cout << "[func2] q: " << *q << ", refcnt=" << q.use_count() << endl;
    *q += 100;
    return q;
}

shared_ptr<Account> func1(shared_ptr<Account> p) {
    cout << "[func1] p: " << *p << ", refcnt=" << p.use_count() << endl;
    *p += 100;
    return func2(p);
}

int main() {
    {
        auto ptr = make_shared<Account>("2222-12345", 250);

        string accno = *ptr;
        cout << "accno: " << accno << endl;

        int val = *ptr;
        cout << "val: " << val << endl;

        *ptr += 300;
        cout << "ptr: " << *ptr << ", refcnt=" << ptr.use_count() << endl;

        {
            auto ptr2 = func1(ptr);
            cout << "ptr2: " << *ptr2 << ", refcnt=" << ptr2.use_count() << endl;
        }

        cout << "ptr: " << *ptr << ", refcnt=" << ptr.use_count() << endl;
    }

    cout << "# accounts: " << Account::numObjs() << endl;
}

