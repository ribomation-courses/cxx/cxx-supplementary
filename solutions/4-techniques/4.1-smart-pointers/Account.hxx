#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include "InstanceCount.hxx"

using namespace std;

class Account : public InstanceCount {
    const string accno;
    int          balance;

public:
    Account(const string& accno, int balance = 100)
            : accno{accno}, balance{balance} {
        cout << "Account{" << accno << ", " << balance << "} @ " << this
             << " [" << InstanceCount::numObjs() << "]" << endl;
    }

    ~Account() {
        cout << "~Account() @ " << this << " [" << (InstanceCount::numObjs() - 1) << "]" << endl;
    }

    Account() = delete;
    Account(const Account&) = delete;
    Account& operator =(const Account&) = delete;

    operator const string() const {
        return accno;
    }

    operator int() const {
        return balance;
    }

    Account& operator +=(int amount) {
        balance += amount;
        return *this;
    }

    string toString() const {
        ostringstream buf;
        buf << "Account{" << accno << ", SEK " << balance << "} [" << InstanceCount::numObjs() << "]";
        return buf.str();
    }

    friend ostream& operator <<(ostream& os, const Account& a) {
        return os << a.toString();
    }

};

