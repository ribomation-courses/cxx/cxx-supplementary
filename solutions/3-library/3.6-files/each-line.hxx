#pragma once
#include <string>
#include <stdexcept>
#include <functional>
#include <fstream>

namespace ribomation::io {
    using namespace std;
    using namespace std::literals;

    inline void each(istream& in, const function<void(string)>& use) {
        for (string line{}; getline(in, line);) use(line);
    }

    inline void each(const string& filename, const function<void(string)>& use) {
        ifstream file{filename};
        if (!file) throw invalid_argument{"Cannot open "s + filename};
        each(file, use);
    }

}

