#include <iostream>
#include <string>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <vector>
#include <cmath>
using namespace std;
using namespace std::literals;

int main(int argc, char** argv) {
    vector<double> numbers{istream_iterator<double>{cin}, istream_iterator<double>{}};
    if (argc > 1 && string{argv[1]} == "-v") {
        copy(numbers.begin(), numbers.end(), ostream_iterator<double>{cout, " "});
        cout << endl;
    }

    auto minval = *min_element(numbers.begin(), numbers.end());
    auto maxval = *max_element(numbers.begin(), numbers.end());
    cout << "min/max: " << minval << " / "s << maxval << endl;

    auto mean = accumulate(numbers.begin(), numbers.end(), 0.0) / numbers.size();
    cout << "mean   : " << mean << endl;

    auto sq     = [](auto x) { return x * x; };
    auto stddev = sqrt(accumulate(numbers.begin(), numbers.end(), 0.0, [=](auto sum, auto x) {
        return sum + sq(x - mean);
    }) / numbers.size());
    cout << "std-dev: " << stddev << endl;

    return 0;
}
