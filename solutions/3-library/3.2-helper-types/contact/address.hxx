#pragma once
#include <string>
#include <optional>

namespace ribomation {
    using namespace std;

    struct Address {
        optional <string>  street;
        optional<unsigned> postCode;
        optional <string>  city;
    };

}

