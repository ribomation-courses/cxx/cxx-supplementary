#pragma once

#include <iosfwd>
#include <string>
#include <optional>
#include "address.hxx"

namespace ribomation {
    using namespace std;

    class Contact {
        string            name;
        optional<string>  email;
        optional<string>  url;
        optional<Address> address;

    public:
        Contact(string name) : name{name} {}

        ~Contact() = default;
        Contact(const Contact&) = default;
        Contact& operator =(const Contact&) = default;

        string getName() const { return name; }

        optional<string> getEmail() const { return email; }

        optional<string> getUrl() const { return url; }

        optional<Address> getAddress() const { return address; }

        Contact& setName(string name) {
            Contact::name = name;
            return *this;
        }

        Contact& setEmail(string email) {
            Contact::email = email;
            return *this;
        }

        Contact& setUrl(string url) {
            Contact::url = url;
            return *this;
        }

        Contact& setAddress(string street, unsigned postCode, string city) {
            Contact::address = {street, postCode, city};
            return *this;
        }

        friend ostream& operator <<(ostream& os, const Contact& c) {
            os << "name=" << c.name;
            if (c.email)
                os << "\nemail=" << c.email.value();
            if (c.url)
                os << "\nurl=" << c.url.value();
            if (c.address)
                os << "\n"
                   << c.address.value().street.value()
                   << ", " << c.address.value().postCode.value()
                   << ", " << c.address.value().city.value();
            return os;
        }
    };

}

