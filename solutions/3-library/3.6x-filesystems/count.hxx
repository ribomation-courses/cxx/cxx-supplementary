#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <utility>

namespace ribomation::io {
    using namespace std;
    using namespace std::literals;
    namespace fs = std::filesystem;

    struct Count {
        const string name;
        unsigned     lines = 0;
        unsigned     words = 0;
        unsigned     chars = 0;

        explicit Count(string name) : name{std::move(name)} {}
        explicit Count(const fs::path& file) : name{file.string()} {
            update(file);
        }

        ~Count() = default;
        Count(const Count&) = default;

        void update(const fs::path& file) {
            chars += fs::file_size(file);

            ifstream f{file};
            for (string line; getline(f, line);) {
                ++lines;
                istringstream buf{line};
                for (string word; buf >> word;) ++words;
            }
        }

        void update(const Count& cnt) {
                lines += cnt.lines;
                words += cnt.words;
                chars += cnt.chars;
        }

        void operator +=(const Count& cnt) {
            update(cnt);
        }

        friend ostream& operator <<(ostream& os, const Count& cnt) {
            return os << setw(6) << cnt.lines
                      << setw(8) << cnt.words
                      << setw(12) << cnt.chars
                      << " " << cnt.name;
        }
    };
}
