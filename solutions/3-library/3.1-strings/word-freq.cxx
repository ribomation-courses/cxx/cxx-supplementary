#include <iostream>
#include <fstream>
#include <string>
#include <string_view>
#include <map>
#include <tuple>
#include <stdexcept>
#include <cstring>
#include <cerrno>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

using namespace std;
using namespace std::literals;

tuple<const char*, size_t>
loadFile(const string& filename) {
    struct stat info{};
    if (stat(filename.data(), &info) == -1) {
        throw invalid_argument{"stat failed: "s + strerror(errno)};
    }
    auto size = info.st_size;

    char* buf = new char[size + 1];
    auto fd = open(filename.data(), O_RDONLY);
    read(fd, buf, static_cast<size_t>(size + 1));
    close(fd);

    return make_tuple(buf, size);
}

map<string_view, unsigned> count(const char* payload, const unsigned long size) {
    map<string_view, unsigned> freqs;

    unsigned long              start = 0;
    do {
        while (!isalpha(payload[start]) && (start < size)) ++start;

        unsigned long end = start;
        while (isalpha(payload[end]) && (start < size)) ++end;

        string_view word{&payload[start], end - start};
        if (word.size() > 3) ++freqs[word];

        start = end + 1;
    } while (start < size);

    return freqs;
}

int main() {
    auto [payload, size] = loadFile("../musketeers.txt"s);
    auto words = count(payload, size);
    for (auto [word, count] : words) {
        cout << word << ": " << count << endl;
    }
    return 0;
}
