#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <stdexcept>

using namespace std;
using namespace std::literals;

void replace(istream& input) {
    regex re{"\\d"};
    auto        lineno = 1U;
    for (string line; getline(input, line); ++lineno) {
        auto result = regex_replace(line, re, "#");
        cout << lineno << ") " << result << endl;
    }
}

int main(int argc, char** argv) {
    if (argc > 1) {
        ifstream f{argv[1]};
        if (!f) throw invalid_argument{"cannot open"s + argv[1]};
        replace(f);
    } else {
        replace(cin);
    }
    return 0;
}

