#include <iostream>
#include <iterator>
#include <algorithm>

using namespace std;

int main() {
    istream_iterator<int> in{cin};
    istream_iterator<int> eof;
    ostream_iterator<int> out{cout, " "};
    transform(in, eof, out, [](auto x) { return x * x; });
    cout << endl;
    return 0;
}
