#include <iostream>
#include <iterator>

using namespace std;

int main() {
    istream_iterator<int> in{cin};
    istream_iterator<int> eof;
    ostream_iterator<int> out{cout, " "};

    for (; in != eof; ++in) {
        const int num = *in;
        *out = num * num;
    }
    cout << endl;
    return 0;
}
