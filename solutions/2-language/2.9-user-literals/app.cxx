#include <iostream>
#include <iomanip>
#include "length.hxx"
using namespace std;
using namespace std::literals;

int main() {
    auto len = 2_m + 2_km - 0.1_mi - 3.5_ya;
    cout << "len: " << fixed << setprecision(2) << len << endl;
    return 0;
}
