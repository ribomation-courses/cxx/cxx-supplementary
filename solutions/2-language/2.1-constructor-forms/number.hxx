#pragma once

#include <iostream>

class Number {
    int value = 0;

public:
    Number(const Number&) = default;
    Number& operator =(const Number&) = default;

    Number(int value) : value{value} {
        std::cout << "Number{" << value << "} @ " << this << std::endl;
    }

    ~Number() {
        std::cout << "~Number{" << value << "} @ " << this << std::endl;
    }

    int getValue() { return value; }

//    operator int() const {
//        std::cout << "operator int(" << value << ") @ " << this << std::endl;
//        return value;
//    }

//    friend std::ostream& operator <<(std::ostream& os, const Number& n) {
//        return os << "Number{" << n.value << "}";
//    }
};

