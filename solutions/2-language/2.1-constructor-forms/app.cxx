#include <iostream>
#include "number.hxx"
using namespace std;
using namespace std::literals;

Number doit(Number n) {
    cout << "[doit] n = " << n.getValue() << endl;
    //operator<<(operator<<(operator<<(cout, "[doit] n = "), n), "\n");

    //((a + b) + c) + d;
    //operator+(operator+(operator+(a, b), c), d);

    Number r{42};
    return r;
}

int main() {
    Number p{17};
    Number n = doit(p);
//    int x = n.operator int();
//    cout << "[main] x = " << x << endl;
    return 0;
}
