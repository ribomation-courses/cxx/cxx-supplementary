#pragma once

#include <iostream>
#include <sstream>
#include <array>
#include <algorithm>
#include <stdexcept>

using namespace std;

class Account {
    int   balance = 0;
    float rate    = 0.75;

    static constexpr unsigned       capacity = 10;
    static array<Account, capacity> storage;
    static array<bool, capacity>    used;

public:
    Account() {
        cerr << "Account() @ " << this << endl;
    }

    Account(int b, float r) : balance{b}, rate{r} {
        cerr << "Account(" << b << ", " << r << ") @ " << this << endl;
    }

    ~Account() {
        cerr << "~Account() @ " << this << endl;
    }

    Account(const Account&) = default;
    Account& operator =(const Account&) = default;

    string toString() const;

    friend ostream& operator <<(ostream& os, const Account& acc) {
        return os << acc.toString();
    }

    static unsigned max() { return capacity; }

    void* operator new(size_t);
    void operator delete(void* ptr);
};

