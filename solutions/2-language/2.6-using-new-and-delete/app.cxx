#include <iostream>
#include <vector>
#include "account.hxx"
using namespace std;
using namespace std::literals;

void singleObject() {
    cout << "---- Single Object ---\n";
    auto ptr = new Account{100, 0.5};
    cout << "ptr: " << *ptr << endl;
    delete ptr;
    cout << "---- END Single Object ---\n";
}

void manyObjects(unsigned n) {
    cout << "---- Many Objects ---\n";

    vector<Account*> accounts;
    while (--n > 0) {
        accounts.push_back(new Account(n * 100 % 3000, static_cast<float>(n * 0.1)));
    }
    for (auto a : accounts) cout << *a << endl;

    cout << "---- Dispose Many Objects ---\n";
    while (!accounts.empty()) {
        auto accPtr = accounts.back();
        accounts.pop_back();
        delete accPtr;
    }

    cout << "---- END Many Objects ---\n";
}

int main() {
    cout << "[main] enter\n";
    singleObject();
    cout << "[main] ------\n";
    manyObjects(Account::max());
    cout << "[main] exit\n";
    return 0;
}
