#include "account.hxx"

array<Account, Account::capacity>    Account::storage{};
array<bool, Account::capacity>       Account::used{};

void* Account::operator new(size_t) {
    cerr << "Account::operator new()\n";

    auto idx = find_if(used.begin(), used.end(), [](auto x) { return !x; });
    if (idx == used.end()) {
        throw overflow_error{"no more slots"};
    }

    *idx = true;
    auto p = distance(used.begin(),idx);
    return &storage[p];
}

void Account::operator delete(void* ptr) {
    cerr << "Account::operator delete()\n";

    auto* PTR = reinterpret_cast<Account*>(ptr);
    auto idx = find_if(storage.begin(), storage.end(), [&](auto& x) {
        return PTR == &x;
    });
    if (idx == storage.end()) {
        throw overflow_error{"cannot find the mem blk"};
    }

    auto p = distance(storage.begin(), idx);
    used[p] = false;
}

string Account::toString() const {
    ostringstream buf;
    buf << "Account{SEK " << balance << ", " << rate << "%} @ " << this;
    return buf.str();
}

