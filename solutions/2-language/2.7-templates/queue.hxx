#pragma once

template<unsigned CAPACITY = 10, typename ElemType = int>
class Queue {
    ElemType storage[CAPACITY];
    int      putIdx = 0;
    int      getIdx = 0;
    int      size   = 0;
public:
    Queue() = default;
    ~Queue() = default;
    Queue(const Queue<CAPACITY, ElemType>&) = delete;
    Queue<CAPACITY, ElemType>& operator =(const Queue<CAPACITY, ElemType>&) = delete;

    bool empty() const { return size == 0; }
    bool full() const { return size == CAPACITY; }

    void put(ElemType x) {
        storage[putIdx++] = x;
        putIdx %= CAPACITY;
        ++size;
    }

    ElemType get() {
        auto x = storage[getIdx++];
        getIdx %= CAPACITY;
        --size;
        return x;
    }
};

