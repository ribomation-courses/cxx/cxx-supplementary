#include <iostream>
#include <string>
#include <complex>
#include "queue.hxx"

using namespace std;
using namespace std::literals;

template<unsigned N, typename T>
void drain(Queue<N, T>& q) {
    while (!q.empty()) cout << q.get() << " ";
    cout << endl;
}

void queue_with_ints() {
    cout << "--- queue<10,int> ---\n";
    Queue<> q;
    for (auto k = 1U; !q.full(); ++k) q.put(k);
    drain(q);
}

void queue_with_many_ints() {
    cout << "--- queue<200,int> ---\n";
    Queue<200> q;
    for (auto k = 1U; !q.full(); ++k) q.put(k);
    drain(q);
}

void queue_with_floating_points() {
    cout << "--- queue<20,double> ---\n";
    Queue<20, double> q;
    for (auto k = 1U; !q.full(); ++k) q.put(k * 0.137);
    drain(q);
}

void queue_with_strings() {
    cout << "--- queue<12,string> ---\n";
    Queue<12, string> q;
    for (auto k = 1U; !q.full(); ++k) q.put("str-"s + to_string(k));
    drain(q);
}

void queue_with_complex() {
    cout << "--- queue<10,complex<double>> ---\n";
    Queue<10, complex<double>> q;
    for (auto k = 1U; !q.full(); ++k) q.put(complex{k * 1.0, k * 0.5});
    drain(q);
}

int main() {
    queue_with_ints();
    queue_with_many_ints();
    queue_with_floating_points();
    queue_with_strings();
    queue_with_complex();
    return 0;
}
