#include <iostream>
#include <string>

using namespace std;
using namespace std::literals;
#define extends 
#define implements 
#define interface struct 
#define abstract 0


interface Stringable {
    virtual string toString() const = abstract;
};

struct Object {
    virtual ~Object() = default;
};

struct Person : extends Object, implements Stringable {
    using super = Object;
    
    explicit Person(string name) : super(), name{move(name)} {
        cout << "Person{" << name << "} @ " << this << endl;
    }

    ~Person() override {
        cout << "~Person{" << name << "} @ " << this << endl;
    }

    string toString() const override {
        return "Person{" + name + "}"s;
    }

private:
    const string name;
};

void use(Object* obj) {
    if (auto p = dynamic_cast<Person*>(obj); obj != nullptr) {
        cout << "[use] p: " << p->toString() << endl;
    }
}

int main() {
    Object* obj = new Person{"Nisse"};
    use(obj);
    delete obj;
    return 0;
}

