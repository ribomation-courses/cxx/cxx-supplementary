#include <iostream>
#include <vector>
#include "Person.hxx"

using namespace std;
using namespace ribomation;

void using_pointers(){
    cout << "--- using pointers" << endl;

    vector<Person*> pers;
    for (unsigned N = 10; N > 0; --N) {
        pers.push_back(new Person{"nisse-" + to_string(N)});
    }
    cout << "# persons: " << Person::getCount() << endl;

    for (auto& p : pers) { cout << p->toString() << endl; }

    cout << "** deleting objs..." << endl;
    for (auto& p : pers) { delete p; }
    cout << "--- leaving func" << endl;
}

void using_objects() {
    cout << "--- using objects" << endl;

    vector<Person> pers;
    for (unsigned N = 10; N > 0; --N) {
        pers.emplace_back("nisse-" + to_string(N));
    }
    cout << "# persons: " << Person::getCount() << endl;

    for (auto& p : pers) { cout << p.toString() << endl; }
    cout << "--- leaving func" << endl;
}

void using_array() {
    cout << "--- using array" << endl;

    auto* arr = new Person[10];
    cout << "# persons: " << Person::getCount() << endl;

    for (auto k=0; k<10; ++k) { cout << arr[k].toString() << endl; }

    cout << "** deleting objs..." << endl;
    delete [] arr;
    //delete  arr;

    cout << "--- leaving func" << endl;
}

int main() {
    cout << "# persons: " << Person::getCount() << endl;
    using_pointers();
    cout << "\n# persons: " << Person::getCount() << endl;
    using_objects();
    cout << "\n# persons: " << Person::getCount() << endl;
    using_array();
    cout << "# persons: " << Person::getCount() << endl;
    return 0;
}
