#pragma once

#include <string>

namespace ribomation {
    using namespace std;

    class Trace {
        string name;
    public:
        Trace(const string& blk = "block") : name{blk} {
            cout << "+ " << name << " @ " << this << endl;
        }

        virtual ~Trace() {
            cout << "- " << name << " @ " << this << endl;
        }
    };

}