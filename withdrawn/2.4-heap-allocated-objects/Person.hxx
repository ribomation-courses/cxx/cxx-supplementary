#pragma  once

#include <string>
#include "InstanceCount.hxx"
#include "trace.hxx"

namespace ribomation {
    using namespace std;

    class Person : public InstanceCount {
        const string name;
        const Trace trace;

    public:
        Person(const string& name) : name{name}, trace{name} {}

        Person() : name{"anon-" + to_string(InstanceCount::getCount())}, trace{name} {}

        const string& getName() const {
            return name;
        }

        string toString() const {
            return "Person{" + name + "}";
        }
    };

}

