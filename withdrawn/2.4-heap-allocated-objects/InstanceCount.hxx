#pragma  once

namespace ribomation {

    class InstanceCount {
        static unsigned count;

    protected:
        InstanceCount() { ++count; }

    public:
        ~InstanceCount() { --count; }
        InstanceCount(const InstanceCount&) {
            ++count;
        }

        static unsigned getCount() { return count; }
    };

}
