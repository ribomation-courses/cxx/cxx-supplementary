#pragma  once

#include <initializer_list>
#include <stdexcept>
#include "type-support.hxx"

namespace ribomation {
    using namespace std;

    template<typename T, unsigned N>
    class SimpleStack {
        T stk[N];
        unsigned top = 0;

    public:
        SimpleStack() = default;

        ~SimpleStack() = default;

        SimpleStack(const SimpleStack<T, N>&) = default;

        SimpleStack<T, N>& operator=(const SimpleStack<T, N>&) = default;

        bool empty() const {
            return top == 0;
        }

        bool full() const {
            return top >= N;
        }

        unsigned size() const {
            return top;
        }

        constexpr unsigned capacity() const {
            return N;
        }

        T pop() {
            if (empty()) {
                throw runtime_error(typeNameOf(*this) + ": empty");
            }
            return stk[--top];
        }

        void push(T x) {
            if (full()) {
                throw runtime_error(typeNameOf(*this) + ": full");
            }
            stk[top++] = x;
        }

        void push(initializer_list<T> args) {
            for (auto it = args.begin(); it != args.end() && !full(); ++it) {
                push(*it);
            }
        }

        SimpleStack<T, N>& operator<<(T x) {
            push(x);
            return *this;
        };

    };

}



