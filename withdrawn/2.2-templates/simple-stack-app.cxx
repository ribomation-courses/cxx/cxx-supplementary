
#include <iostream>
#include <string>
#include <complex>
#include "SimpleStack.hxx"

using namespace std;
using namespace ribomation;

template<typename T, unsigned N>
void drain(SimpleStack<T, N>& stk) {
    while (!stk.empty()) cout << stk.pop() << " ";
    cout << endl;
}

void stack_with_integral_numbers() {
    cout << "--- Stack<int, 32> ---\n";
    SimpleStack<int, 32> stk;

    for (int k = 0; !stk.full(); k += 3) stk.push(k);
    drain(stk);
}

void stack_with_text_strings() {
    cout << "--- Stack<string, 10> ---\n";
    SimpleStack<string, 10> stk;

    for (int k = 0; !stk.full(); k += 2) stk.push("str-" + to_string(k));
    drain(stk);
}

void stack_with_complex_numbers() {
    cout << "--- Stack<complex<double>, 10> ---\n";
    SimpleStack<complex<double>, 10> stk;

    for (int k = 0; !stk.full(); k += 2) {
        complex<double> z{2.0 * k, 1.0 * k};
        stk.push(z);
    }
    drain(stk);
}

void stack_with_init_lst() {
    cout << "--- Stack<int, 10> w init-lst ---\n";
    SimpleStack<int, 10> stk;

    stk.push({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    drain(stk);
}

void stack_with_shift_ops() {
    cout << "--- Stack<int, 10> w shift-op ---\n";
    SimpleStack<int, 20> stk;

    for (int k = 0; stk.size() < (stk.capacity() - 1); k += 3) {
        stk << k << 333;
    }
    drain(stk);
}

void stack_with_overflow() {
    cout << "--- Stack<int, 2> w overflow ---\n";
    SimpleStack<int, 1> stk;

    stk << 42;
    stk << 42;
}

void stack_with_underflow() {
    cout << "--- Stack<int, 2> w underflow ---\n";
    SimpleStack<string, 2> stk;

    stk.pop();
}

int main() {
    stack_with_integral_numbers();
    stack_with_text_strings();
    stack_with_complex_numbers();
    stack_with_init_lst();
    stack_with_shift_ops();

    try {
        stack_with_overflow();
    } catch (runtime_error& x) {
        cout << "ERROR: " << x.what() << endl;
    }

    try {
        stack_with_underflow();
    } catch (runtime_error& x) {
        cout << "ERROR: " << x.what() << endl;
    }

    return 0;
}
