#pragma  once

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace ribomation {
    using namespace std;

    class Customer {
        string id;
        string name;
        string x;
        string y;

    public:
        Customer(unsigned id, const string& name)
                : id("Cust-" + to_string(id*100)), name(name) {
            cout << "CREATE " << toString() << " @ " << this << endl;
        }

        virtual ~Customer() {
            cout << "DISPOSE " << toString() << " @ " << this << endl;
        }

        string toString() const {
            ostringstream buf;
            buf << "customer{" << id << ", " << name << "}";
            return buf.str();
        }
    };

    struct CustomerFactory {
        vector<string> names{
                "Anna Conda", "Per Silja", "Inge Vidare", "Sham Poo"
        };
        unsigned       nextCustomerId = 0;

        Customer create() {
            auto id = nextCustomerId++;
            return {id, names[id % names.size()]};
        }
    };

}
