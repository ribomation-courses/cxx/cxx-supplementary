cmake_minimum_required(VERSION 3.10)
project(2_2_templates)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors)

add_executable(simple-stack
        type-support.hxx
        SimpleStack.hxx
        simple-stack-app.cxx
        )

add_executable(object-factory
        ObjectFactory.hxx
        Customer.hxx
        Account.hxx
        object-factory-app.cxx
        )


add_executable(type-support-test
        type-support.hxx
        type-support-test.cxx
        )
