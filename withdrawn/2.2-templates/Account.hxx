#pragma  once

#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>

namespace ribomation {
    using namespace std;

    class Account {
        string id;
        double balance;

    public:
        Account(unsigned id, double balance)
                : id("1234-" + to_string(id)), balance(balance) {
            cout << "CREATE " << toString() << " @ " << this << endl;
        }

        virtual ~Account() {
            cout << "DISPOSE " << toString() << " @ " << this << endl;
        }

        string toString() const {
            ostringstream buf;
            buf << "account{" << id << ", SEK " << fixed << setprecision(2) << balance << "}";
            return buf.str();
        }
    };

    struct AccountFactory {
        unsigned nextAccountId = 0;

        Account create() {
            nextAccountId++;
            return {nextAccountId, nextAccountId * 97 * 17.5};
        }
    };

}
