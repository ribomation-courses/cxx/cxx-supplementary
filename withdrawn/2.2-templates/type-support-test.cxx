
#include <iostream>
#include "type-support.hxx"
#include "SimpleStack.hxx"

using namespace std;
using namespace ribomation;

int main() {
    int n = 42;
    const int& r = n;
    cout << "typeof(x): " << typeid(r).name() << " --> " << typeNameOf(r) << endl;

    SimpleStack<int, 21> s;
    cout << "typeof(s): " << typeid(s).name() << " --> " << typeNameOf(s) << endl;

    SimpleStack<string, 42> s2;
    cout << "typeof(s2): " << typeid(s2).name() << " --> " << typeNameOf(s2) << endl;

    return 0;
}
