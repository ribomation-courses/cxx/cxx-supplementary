#pragma  once

namespace ribomation {

    template<typename Factory>
    auto createObject(Factory& f) -> decltype(f.create()) {
        return f.create();
    }

}
