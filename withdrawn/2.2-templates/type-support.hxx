#pragma  once
#ifdef __GNUG__

#include <string>
#include <typeinfo>
#include <cstdlib>
#include <memory>
#include <cxxabi.h>
#include <stdexcept>
#include <regex>

namespace ribomation {
    using namespace std;

    string demangle(const char* name) {
        int status = -4;
        std::unique_ptr<char, void (*)(void*)> res{
                abi::__cxa_demangle(name, NULL, NULL, &status), free
        };
        if (status == 0) {
            auto type = res.get();

            string RE = "std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >";
            if (!regex_match(type, regex{".*" + RE + ".*"})) {
                return type;
            }

            return regex_replace(type, regex{RE}, "string");
        }

        if (status == -1) throw runtime_error("Memory allocation failure occurred");
        if (status == -2) throw runtime_error("Not a valid name under the C++ ABI mangling rules");
        if (status == -3) throw runtime_error("One of the arguments is invalid");
        throw runtime_error("fatal error");
    }

    template<typename T>
    std::string typeNameOf(const T& t) {
        return demangle(typeid(t).name());
    }
}
#else
#   error "Not a GCC compiler"
#endif

