#include <iostream>
#include "Account.hxx"
#include "Customer.hxx"
#include "ObjectFactory.hxx"

using namespace std;
using namespace ribomation;

int main() {
    unsigned        N = 32;
    AccountFactory  af;
    CustomerFactory cf;

    for (unsigned k = 1; k <= N; ++k) {
        cout << "[object-" << k << "] ";
        if (k % 2) {
            cout << "# " << k << ": " << createObject(af).toString() << endl;
        } else {
            cout << "# " << k << ": " << createObject(cf).toString() << endl;
        }
        cout << "-------------\n";
    }

    return 0;
}
