#ifndef __INSTANCECOUNT__
#define __INSTANCECOUNT__

class InstanceCount {
	static int		count;

	protected:
	InstanceCount() {count++;}
	~InstanceCount() {count--;}

	public:
	static int  numObjs() {return count;}
};

#endif
