#include <iostream>
#include <string>
#include "SimpleUnitTest.hpp"
#include "LineSlurper.hpp"

using namespace std;

int main() {
    const string filename = "./LineSlurper.hpp";

    {
        unsigned  lineCnt = 0;
        for (auto line : LineSlurper(filename)) { ++lineCnt; }
        assertEquals(lineCnt, 46);
    }

    {
        string content = "";
        for (auto line : LineSlurper(filename)) { content += line; }
        assertEquals(content.size(), 1007);
    }

    return testResult();
}
