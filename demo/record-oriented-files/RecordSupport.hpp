#pragma once
#include <string>
#include <cstring>
#include "StringUtils.hpp"
using namespace std;


static void* toStorage(string& variable, unsigned numBytes, void* offset) {
    memset(offset, '\0', numBytes);
    variable.copy(static_cast<char*>(offset), numBytes, 0);
    return offset + numBytes;
}

template<typename NumericType>
static void* toStorage(NumericType value, void* offset) {
    memcpy(offset, &value, sizeof(NumericType));
    return offset + sizeof(NumericType);
}

static void* fromStorage(string& variable, unsigned numBytes, void* offset) {
    string buf(static_cast<char*>(offset), numBytes);
    StringUtils s;
    variable = s.trim(buf);
    return offset + numBytes;
}

template<typename NumericType>
static void* fromStorage(NumericType& variable, void* offset) {
    memcpy(&variable, offset, sizeof(NumericType));
    return offset + sizeof(NumericType);
}
