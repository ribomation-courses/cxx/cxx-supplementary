#include "SimpleUnitTest.hpp"
#include "RecordOrientedFile.hpp"
#include "Person.hpp"
using namespace std;


int main(int numArgs, char* args[]) {
    const unsigned N        = 100'000;
    const string   filename = "persons.db";
    rm(filename);
    touch(filename);

    string names;
    string ages;
    string weights;
    {
        RecordOrientedFile<Person> db{filename};

        for (int k = 0; k < N; ++k) {
            Person p = mk();
            //cout << p.toString() << endl;
            
            names += p.name() + ",";
            ages += to_string(p.getAge()) + ",";
            weights += to_string(p.getWeight()) + ",";
            
            db << p;
        }
        assertEquals(db.size(), N * Person::SIZE);
    }
    
    {
        RecordOrientedFile<Person> db{filename};

        string names2;
        string ages2;
        string weights2;

        for (auto p : db) {
            //cout << p.toString() << endl;
            
            names2 += p.name() + ",";
            ages2 += to_string(p.getAge()) + ",";
            weights2 += to_string(p.getWeight()) + ",";
        }
        assertEquals(names2, names);
        assertEquals(ages2, ages);
        assertEquals(weights2, weights);
    }

    return testResult();
}
