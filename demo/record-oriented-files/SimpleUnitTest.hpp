#pragma once

#include <iostream>
#include <string>
#include <vector>

using namespace std;

static unsigned TEST_failure_count = 0;

vector<string> mk(initializer_list<string> s) {
    return {s.begin(), s.end()};
}

ostream& operator<<(ostream& os, const vector<string>& v) {
    bool      first = true;
    for (auto item : v) {
        if (first) first = false; else os << ",";
        os << item;
    }
    return os;
}

void _location(const string& file, int line, const string& func) {
    cout << "  " << func << ":" << line << " (" << file << ")" << endl;
}

void _assertEquals(const string& actual, const string& expected, const string& actualStr, const string& expectedStr,
                   const string& func, int lineno, const string& file) {
    if (actual == expected) return;

    ++TEST_failure_count;
    cout << "failed: " << actualStr << " \"" << actual << "\" != " << expectedStr << " \"" << expected << "\"" << endl;
    _location(file, lineno, func);
}

void _assertEquals(long actual, long expected, const string& actualStr, const string& expectedStr,
                   const string& func, int lineno, const string& file) {
    if (actual == expected) return;

    ++TEST_failure_count;
    cout << "failed: " << actualStr << " (" << actual << ") != " << expected << endl;
    _location(file, lineno, func);
}

void _assertEquals(const vector<string>& actual, const vector<string>& expected, const string& actualStr,
                   const string& expectedStr, const string& func, int lineno, const string& file) {
    if (actual == expected) return;

    ++TEST_failure_count;
    cout << "failed: " << " [" << actual << "] != [" << expected << "]" << endl;
    _location(file, lineno, func);
}


#define assertEquals(actual, expected) _assertEquals((actual), (expected), #actual, #expected, __PRETTY_FUNCTION__, __LINE__, __FILE__)


unsigned testResult() {
    if (TEST_failure_count == 0) {
        cout << "All tests passed" << endl;
    } else {
        cout << "# test failures = " << TEST_failure_count << endl;
    }
    return TEST_failure_count;
}
