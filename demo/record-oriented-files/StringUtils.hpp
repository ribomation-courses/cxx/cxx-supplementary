#pragma once

#include <string>
#include <vector>
#include <ctime>
#include <cstring>

using namespace std;

struct StringUtils {
    char FILLER = ' ';

    string trim(const string& txt) {
        string result;
        for (unsigned k = 0; k < txt.size(); ++k) {
            if (txt[k]) result += txt[k];
        }
        return result;
    }

    vector<string>
    split(const string& txt, const char delim) {
        vector<string> result;
        string         word = "";
        for (unsigned  k    = 0; k < txt.size(); ++k) {
            if (txt[k] != delim) {
                word += txt[k];
            } else {
                result.push_back(word);
                word = "";
            }
        }
        if (!word.empty()) result.push_back(word);
        return result;
    }

    string formatDate(const string& fmt, time_t timestamp) {
        char buf[1024];
        memset(buf, '\0', sizeof(buf));
        strftime(buf, sizeof(buf), fmt.c_str(), localtime(&timestamp));
        return buf;
    }

    string formatDate(time_t timestamp) {
        return formatDate("%F %T", timestamp); // 2015-03-25 09:33:10
    }

    time_t now() {
        return time(nullptr);
    }

    string today(const string& fmt) {
        return formatDate(fmt, now());
    }

    string today() {
        return formatDate(now());
    }

};