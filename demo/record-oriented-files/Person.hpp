#pragma once
// This HPP is only intended for testing

#include <iostream>
#include <string>
#include <random>
#include <cstdlib>
#include "RecordSupport.hpp"

using namespace std;


class Person {
    string fname;
    string lname;
    int    age;
    double weight;

public:
    static const unsigned NAME = 12;
    static const unsigned SIZE = 2 * NAME + sizeof(int) + sizeof(double);

    Person(const string& fname, const string& lname, int age, double weight)
            : fname(fname), lname(lname), age(age), weight(weight) { }

    Person() = default;

    void toExternal(char* storage) {
        void* offset = storage;
        offset = toStorage(fname, NAME, offset);
        offset = toStorage(lname, NAME, offset);
        offset = toStorage(age, offset);
        offset = toStorage(weight, offset);
    }

    void fromExternal(char* storage) {
        fname = "";
        lname = "";
        age   = 0;
        weight = 0;

        void* offset = storage;
        offset = fromStorage(fname, NAME, offset);
        offset = fromStorage(lname, NAME, offset);
        offset = fromStorage(age, offset);
        offset = fromStorage(weight, offset);
    }

    string name() const { return fname + " " + lname; }

    int getAge() const { return age; }

    double getWeight() const { return weight; }

    string indexKey() const { return name(); }

    string toString() const {
        return name() + "/" + to_string(getAge()) + "/" + to_string(getWeight());
    }
};


Person mk() {
    static default_random_engine             r;
    static vector<string>                    names = {"Anna", "Bertil", "Carin", "David", "Eva", "Fredrik", "Gun",
                                                      "Hans", "Ida", "Jens", "Katrin", "Lars"};
    static uniform_int_distribution<size_t>  nextName{0, names.size() - 1};
    static uniform_int_distribution<int>     nextAge{20, 70};
    static uniform_real_distribution<double> nextWeight{50, 120};

    string fname  = names[nextName(r)];
    string lname  = names[nextName(r)] + "son";
    int    age    = nextAge(r);
    double weight = nextWeight(r);

    return {fname, lname, age, weight};
}


void rm(const string& file) {
    system(("/bin/rm -f " + file).c_str());
}

void touch(const string& file) {
    system(("/usr/bin/touch " + file).c_str());
}
