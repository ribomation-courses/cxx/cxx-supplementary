#pragma once

#include <fstream>
#include <iomanip>
#include <unordered_map>
#include <exception>
#include "StringUtils.hpp"

using namespace std;

/**********************************
Requirements of type Record
struct MyRecord {
    static const unsigned  SIZE
    
    void    fromExternal(char* storage)
    void    toExternal(char* storage)
    string  indexKey() const
};
***********************************/


// --------------------------
// --- RecordOrientedFile ---
template<typename Record>
class RecordOrientedFile {
    string                          filename;
    fstream                         file;
    unordered_map<string, unsigned> index;
    unsigned                        lastPosition = 0;
    bool                            indexBuilt   = false;

    void store(Record& record, unsigned pos) {
        char buf[Record::SIZE];
        record.toExternal(buf);
        file.seekp(pos * Record::SIZE);
        file.write(buf, Record::SIZE);
        file.flush();
    }

    void load(Record& record, unsigned pos) {
        char buf[Record::SIZE];
        file.sync();
        file.seekg(pos * Record::SIZE);
        file.read(buf, Record::SIZE);
        record.fromExternal(buf);
    }

public:
    RecordOrientedFile(const string& filename)
            : filename(filename),
              file(filename, ios_base::in | ios_base::out | ios_base::binary) {
        if (!file) throw invalid_argument("not found: " + filename);
    }

    ~RecordOrientedFile() = default;

    RecordOrientedFile() = delete;

    RecordOrientedFile(const RecordOrientedFile<Record>&) = delete;

    RecordOrientedFile<Record>& operator =(const RecordOrientedFile<Record>&) = delete;

    unsigned size() {
        file.seekg(0, ios_base::end);
        return file.tellg();
    }

    unsigned count() {
        return size() / Record::SIZE;
    }

    void reset() {
        lastPosition = 0;
        file.seekg(0);
        file.seekp(0);
        indexBuilt = false;
        index.clear();
    }

    void dumpIndex() {
        cerr << "--- RoF Index ---\n";
        for (auto idx : index) {
            cerr << idx.first << "=" << idx.second << endl;
        }
        cerr << "---END---\n";
    }

    void buildIndex() {
        index.clear();
        unsigned  pos = 0;
        for (auto r : *this) index[r.indexKey()] = pos++;
        indexBuilt = true;
    }

    RecordOrientedFile<Record>& operator <<(Record& record) {
        store(record, count()); //store after last --> append
        return *this;
    }

    RecordOrientedFile<Record>& operator >>(Record& record) {
        load(record, lastPosition++);
        return *this;
    }

    operator bool() {
        return lastPosition < count();
    }

    // --- for-each support ---
    struct iterator {
        RecordOrientedFile<Record>& rof;
        unsigned currentPosition;

        iterator(RecordOrientedFile<Record>& rof, unsigned currentPosition)
                : rof(rof), currentPosition(currentPosition) { }

        bool operator !=(const iterator& that) {
            return (*this).currentPosition != that.currentPosition;
        }

        iterator& operator ++() {
            ++currentPosition;
            return *this;
        }

        Record operator *() {
            Record r;
            rof.load(r, currentPosition);
            return r;
        }
    };

    iterator begin() { return {*this, 0}; }

    iterator end() { return {*this, count()}; }


    // --- index operator support ---
    struct Position {
        RecordOrientedFile<Record>& rof;
        unsigned currentPosition;

        Position(RecordOrientedFile& rof, unsigned int currentPosition)
                : rof(rof), currentPosition(currentPosition) { }

        Position& operator =(Record& r) {
            rof.store(r, currentPosition);
            return *this;
        }

        operator Record() {
            Record r;
            rof.load(r, currentPosition);
            return r;
        }
    };

    Position operator [](unsigned position) {
        if (position < 0) throw out_of_range("negative pos=" + to_string(position));
        if (position > count())
            throw out_of_range("too large pos=" + to_string(position) + ", count=" + to_string(count()));
        return {*this, position};
    }

    Position operator [](const string& key) {
        auto position = index.find(key); //returns a pair<string,unsigned>
        if (position == index.end()) throw out_of_range("not found key=" + key);

        if (!indexBuilt) { buildIndex(); }

        return (*this)[position->second]; //second --> index-position
    }

};

