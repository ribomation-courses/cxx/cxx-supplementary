#include "SimpleUnitTest.hpp"
#include "RecordOrientedMemoryMappedFile.hpp"
#include "Person.hpp"
using namespace std;


int main(int numArgs, char* args[]) {
    const unsigned N        = 1000000;
    const string   filename = "persons2.db";
    rm(filename);

    string names;
    string ages;
    string weights;
    {
        RecordOrientedMemoryMappedFile<Person> db{filename, N};
        db.setFlushEveryWrite(false);

        for (int k = 0; k < N; ++k) {
            Person p = mk();
            //cout << p.toString() << endl;

            names += p.name() + ",";
            ages += to_string(p.getAge()) + ",";
            weights += to_string(p.getWeight()) + ",";

            db[k] = p;
        }
        db.flush();
    }

    {
        RecordOrientedMemoryMappedFile<Person> db{filename};
        assertEquals(db.size(), N * Person::SIZE);
        assertEquals(db.count(), N);

        string names2;
        string ages2;
        string weights2;

        for (int k = 0; k < N; ++k) {
            Person p = db[k];
            //cout << p.toString() << endl;

            names2 += p.name() + ",";
            ages2 += to_string(p.getAge()) + ",";
            weights2 += to_string(p.getWeight()) + ",";
        }
        assertEquals(names2, names);
        assertEquals(ages2, ages);
        assertEquals(weights2, weights);
    }

    return testResult();
}
