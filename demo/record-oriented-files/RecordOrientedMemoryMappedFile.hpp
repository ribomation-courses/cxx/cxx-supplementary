#pragma once

#include <string>
#include <cstddef>
#include <cstdio>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdexcept>

using namespace std;

extern "C" {
//int open(const char *pathname, int flags);
int close(int fd);
int stat(const char* path, struct stat* buf);
void* mmap(void* addr, size_t length, int prot, int flags, int fd, off_t pgoffset);
};

template<typename Record>
class RecordOrientedMemoryMappedFile {
    string filename;
    void* payload;
    size_t payloadSize;
    Record singletonRecord;
    bool   flushEveryWrite = true;

    size_t fileSize(const string& filename) {
        struct stat fileInfo;
        if (stat(filename.c_str(), &fileInfo) < 0) {
            perror("fileSize");
            throw invalid_argument("Cannot stat " + filename);
        }
        return (size_t) fileInfo.st_size;
    }

    bool exists(const string& filename) {
        struct stat fileInfo;
        if (stat(filename.c_str(), &fileInfo) == 0) return true;
        if (errno == ENOENT) return false;
        throw invalid_argument(strerror(errno));
    }

    void* mapFile(int fd, size_t numBytes, const string& path) {
        void* payload = (char*) mmap(0, numBytes, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (payload == MAP_FAILED) throw invalid_argument("failed to mmap " + path);
        close(fd);
        return payload;
    }

public:
    RecordOrientedMemoryMappedFile(const string& filename) : filename(filename) {
        if (!exists(filename)) throw invalid_argument(filename + " not found");

        payloadSize = fileSize(filename);

        int fd = open(filename.c_str(), O_RDWR);
        if (fd < 0) throw invalid_argument("cannot open " + filename);

        payload = mapFile(fd, payloadSize, filename);
    }

    RecordOrientedMemoryMappedFile(const string& filename, unsigned numRecords) : filename(filename) {
        if (exists(filename)) throw invalid_argument(filename + " exists");

        payloadSize = numRecords * Record::SIZE;

        int fd = creat(filename.c_str(), S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
        if (fd < 0) throw invalid_argument("failed to create " + filename);
        if (ftruncate(fd, payloadSize) < 0) throw invalid_argument(strerror(errno));
        close(fd);

        fd = open(filename.c_str(), O_RDWR);
        if (fd < 0) throw invalid_argument("cannot open " + filename);

        payload = mapFile(fd, payloadSize, filename);
    }

    ~RecordOrientedMemoryMappedFile() {
        munmap(payload, payloadSize);
    }

    RecordOrientedMemoryMappedFile() = delete;

    RecordOrientedMemoryMappedFile(const RecordOrientedMemoryMappedFile<Record>&) = delete;

    RecordOrientedMemoryMappedFile<Record>& operator=(const RecordOrientedMemoryMappedFile<Record>&) = delete;

    void flush() {
        msync(payload, payloadSize, MS_SYNC);
    }

    unsigned size() const {
        return (unsigned int) payloadSize;
    }

    unsigned count() const {
        return size() / Record::SIZE;
    }

    bool isFlushEveryWrite() const {
        return flushEveryWrite;
    }

    void setFlushEveryWrite(bool flushEveryWrite) {
        RecordOrientedMemoryMappedFile::flushEveryWrite = flushEveryWrite;
    }

    struct Position {
        RecordOrientedMemoryMappedFile<Record>& db;
        void                                  * recordAddr;

        Position(RecordOrientedMemoryMappedFile<Record>& db, void* recordAddr)
                : db(db), recordAddr(recordAddr) { }

        operator Record&() {
            db.singletonRecord.fromExternal(reinterpret_cast<char*>(recordAddr));
            return db.singletonRecord;
        }

        Position& operator=(Record& recordToWrite) {
            db.singletonRecord = recordToWrite;
            db.singletonRecord.toExternal(reinterpret_cast<char*>(recordAddr));
            if (db.isFlushEveryWrite()) db.flush();
            return *this;
        }

    };

    Position operator[](unsigned position) {
        if (position < 0) throw invalid_argument("negative index");
        if (position >= count()) throw invalid_argument("index out of bounds: " + to_string(position));

        return {*this, payload + position * Record::SIZE};
    }

};
